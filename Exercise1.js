var essay = "Essay jenis ini mempunyai tujuan untuk meyakinkan pembaca. Melalui serangkaian argument yang ditulis, diharapkan pembaca dapat menerima ide, pandangan, sikap, maupun kepercayaan si penulis terhadap isu atau permasalahan yang diangkat.";
var panjang = essay.length;

console.log("jumlah huruf dalam essay adalah " + panjang + " huruf.");

console.log("");

const cities = ["Jakarta", "Solo", "Surabaya", "Bogor"];
console.log(cities.map(c => c.length))
console.log(cities.map(c => c.length).length)
console.log(cities.map(c => c.length).filter(c => c > 5))
console.log(cities.map(c => c.length).filter(c => c > 5).length)
console.log("mengurutkan cities berdasarkan yang terkecil " + cities.map(c => c.length).sort())

console.log("Ascending : " + cities.map(c => c.length).sort(function (a, b) { return a - b }))
console.log("Descending : " + cities.map(c => c.length).sort(function (a, b) { return b - a }))

const lenfun = function (word) {
    return word.length
}

console.log("");

const animal = ["Turtle", "Bird"];
animal.map(lenfun);
console.log(animal.map(a => a.length))

console.log("");




